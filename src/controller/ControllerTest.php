<?php

namespace Drupal\cloudimage\controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;

/**
 * Get All Project.
 */
class ControllerTest extends ControllerBase {

  public function test() {
    return \Drupal::service('cloudimage.cdnscaleflex')->exportImageUrls();
  }

}