<?php


namespace Drupal\cloudimage\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ResourceTypeForm.
 *
 * @package Drupal\kam_spotlight\Form
 */
class CloudimageForm extends ConfigFormBase {

  private $cloudimageConfig;

  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->cloudimageConfig = $this->config('cloudimage.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudimage_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [ 'cloudimage.settings' ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cloudimage.settings');
    $form['#title'] = $this->t('Cloud Image Settings');
    $form['client_key']= [
        '#type' => 'textfield',
        '#title' => 'Please enter the client key',
        '#default_value' => $config->get('client_key'),
        '#description' => t("X-Client-Key is the client's API key (can be found in Admin Console left menu : Settings / CDN Invalidation)."),
      ];

      $form['token']= [
        '#type' => 'textfield',
        '#title' => 'Please enter the token',
        '#default_value' => $config->get('token'),
        '#description' => t("Your token"),
      ];
      $form['url_site']= [
        '#type' => 'textfield',
        '#title' => 'Please enter the website URL',
        '#default_value' => $config->get('url_site'),
        '#description' => t("Your website URL <i>https://www.example.com</i>"),
      ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->cloudimageConfig->set('client_key', $form_state->getValue('client_key'));
    $this->cloudimageConfig->set('token', $form_state->getValue('token'));
    $this->cloudimageConfig->set('url_site', $form_state->getValue('url_site'));
    $this->cloudimageConfig->save();
    parent::submitForm($form, $form_state);
  }
}
