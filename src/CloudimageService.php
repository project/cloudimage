<?php

namespace Drupal\cloudimage;

use Drupal;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

class CloudimageService {

  /**
   * Drupal logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger, FileSystemInterface $file_system) {
    $this->getCloudimgSettings = $config_factory->get('cloudimage.settings');
    $this->logger = $logger->get('cloudimage');
    $this->fileSystem = $file_system;
  }

  public function exportImageUrls() {
    $config = $this->getCloudimgSettings;
    $url_base = \Drupal::service('router.request_context')->getCompleteBaseUrl();
    $dir = 'public://';
    if(!empty($this->fileSystem->scanDirectory($dir, '/\.(gif|jpg|jpeg|png|svg)$/i')))
    {
      $files = $this->fileSystem->scanDirectory($dir, '/\.(gif|jpg|jpeg|png|svg)$/i');
      $images = [];
      foreach ($files as $file) {
        if(!empty($config->get('url_site')))
        {
          $image_path = str_replace($url_base, $config->get('url_site') , file_create_url($file->uri));
        }
        else{
          $image_path = file_create_url($file->uri);
        }
        $images[] = $image_path;
      }
      $response = new Response();
      $client = new Client();
      $response = $client->post('https://warmup.api.cloudimage.com/warmup/urls', [
        'headers' => [
          'X-Client-Key' => $config->get('client_key'),
          'Content-Type' => 'application/json',
        ],
        'json' => [
          'data' => $images,
        ],
      ]);
      $message = "{ \n" . "status".":"."success".",\n"."count_added".":". count($images) ."\n }";
      $this->logger->notice($message);
      return $response;
    }
  }

}